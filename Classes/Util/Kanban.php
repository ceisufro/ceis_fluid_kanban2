<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 17-10-2016
 * Time: 9:58
 */

namespace Francisco\DragAndDropV2\Util;


class Kanban
{

    private $listaTarjetas = array();
    private $listaEstados = array();


    /**
     * @param string $uidTarjeta
     * @param string $uidApp
     * @param string $codigoTarjeta
     * @param string $descripcionTarjeta
     * @param string $descripcionAlternativa
     * @param string $fechaCreacion
     * @param string $estado
     * @param string $nombreResponsable
     * @param string $mailResponsable
     * @param string $fechaUltimaActualizacion
     * @param string $link
     * @param string $colorTarjeta
     */
    public function agregarTarjeta($uidTarjeta, $uidApp = null, $codigoTarjeta = null, $descripcionTarjeta = null, $descripcionAlternativa = null, $fechaCreacion = null, $estado, $nombreResponsable = null, $mailResponsable = null, $fechaUltimaActualizacion = null, $link = null, $colorTarjeta = null) {

        $nuevaTarjeta = array();

        $nuevaTarjeta['tarjeta']['uidTarjeta'] = $uidTarjeta;
        $nuevaTarjeta['tarjeta']['uidApp'] = $uidApp;
        $nuevaTarjeta['tarjeta']['codigoTarjeta'] = $codigoTarjeta;
        $nuevaTarjeta['tarjeta']['descripcionTarjeta'] = $descripcionTarjeta;
        $nuevaTarjeta['tarjeta']['descripcionAlternativa'] = $descripcionAlternativa;
        $nuevaTarjeta['tarjeta']['fechaCreacion'] = $fechaCreacion;
        $nuevaTarjeta['tarjeta']['estado'] = $estado;
        $nuevaTarjeta['tarjeta']['nombreResponsable'] = $nombreResponsable;
        $nuevaTarjeta['tarjeta']['mailResponsable'] = $mailResponsable;
        $nuevaTarjeta['tarjeta']['fechaUltimaActualizacion'] = $fechaUltimaActualizacion;
        $nuevaTarjeta['tarjeta']['link'] = $link;
        $nuevaTarjeta['tarjeta']['colorTarjeta'] = $colorTarjeta;

        $this->listaTarjetas[] = $nuevaTarjeta;
        //$this->listaTarjetas['tarjeta']
    }


    public function agregarEstado($nombre, $descripcion = null, $colorColumna = null) {
        $nuevoEstado = array();

        $nuevoEstado['estado']['nombre'] = $nombre;
        $nuevoEstado['estado']['descripcion'] = $descripcion;
        $nuevoEstado['estado']['colorColumna'] = $colorColumna;

        $this->listaEstados[] = $nuevoEstado;
    }

    /**
     * @return array
     */
    public function getListaTarjetas()
    {
        return $this->listaTarjetas;
    }

    /**
     * @return array
     */
    public function getListaEstados() {
        return $this->listaEstados;
    }

    /**
     * @return string
     */
    public function toString() {
        $cadena = "";

        foreach ($this->listaTarjetas as $tarjeta) {
            $cadena = $cadena."Codigo: ".$tarjeta['tarjeta']['codigoTarjeta']." - Descripcion: ".$tarjeta['tarjeta']['descripcionTarjeta']." - Estado: ".$tarjeta['tarjeta']['estado']." ";
        }

        return $cadena;
    }

}
