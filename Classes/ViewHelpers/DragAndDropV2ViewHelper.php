<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 05-04-16
 * Time: 16:30
 */

namespace Francisco\DragAndDropV2\ViewHelpers;

use TYPO3\CMS\Fluid\ViewHelpers\Form\AbstractFormViewHelper;
use TYPO3\CMS\Fluid\View\StandaloneView;
use Francisco\DragAndDropV2\Util\Tarjeta;


class DragAndDropV2ViewHelper extends AbstractFormViewHelper {

    /**
     * @param string $tituloProyecto
     * @param string $colorKanban
     * @param boolean $dragAndDrop
     * @param string $servicioCambioEstado
     * @param array $objEstados
     * @param array $objTarjetas
     * @return string
     */
    public function render($tituloProyecto = null, $colorKanban = null, $dragAndDrop = null, $servicioCambioEstado = null, $objEstados, $objTarjetas) {
        $directorioVista = PATH_site."typo3conf/ext/drag_and_drop_v2/Resources/Private/Templates/VistaPrincipal/";
        $rutaCompletaVista = $directorioVista.'vistaElementos.html';
        $vista = new StandaloneView();
        $vista->setFormat("html");
        $vista->setTemplatePathAndFilename($rutaCompletaVista);

        if ($dragAndDrop == "" || $dragAndDrop == 0 || $dragAndDrop == null) {
            $habilitaDaD = "";
            error_log("DaD deshabilitado");
        } else {
            $habilitaDaD = "connectedSortable";
            error_log("DaD Habilitado");
        }

        if ($tituloProyecto == "") {
            $contenedorPrincipal = "false";
        } else {
            $contenedorPrincipal = "true";
        }

        $vista->assign('tituloProyecto',$tituloProyecto);
        $vista->assign('colorKanban',$colorKanban);
        $vista->assign('objEstados',$objEstados);
        $vista->assign('objTarjetas',$objTarjetas);
        $vista->assign("habilitaDaD",$habilitaDaD);
        $vista->assign("servicioCambioEstado",$servicioCambioEstado);
        $vista->assign("habilitaContPrincipal",$contenedorPrincipal);

        return $vista->render();
    }

}