/**
 * Created by Javier on 06-04-16.
 */

//Permite desplazar elementos arrastrables a los div con clase connectedSortable

iniciarDragAndDrop();

function iniciarDragAndDrop() {

    $(function() {
        $('.connectedSortable').sortable({
            connectWith: ".connectedSortable", //Permite desplazar elementos arrastrables a los div con clase connectedSortable

            receive: function(event, ui) {

                var idColumna = $(this).parent().parent().attr("id");

                var data = {};

                var estado = $("#"+idColumna).children().children()[0].getAttribute("kanban_nombre_estado");

                var uidEstado = $("#"+idColumna).children().children()[1].getAttribute("kanban_uid_estado");

                var servicioCambioEstado = $("#"+idColumna).children().children()[2].getAttribute("kanban_servicio_estado");

                var uidTarjeta = ui.item.children()[0].querySelector('[kanban_uid_tarjeta]');

                var codigoTarjeta = ui.item.children()[0].querySelector('[kanban_codigo_tarjeta]');

                var uidApp = ui.item.children()[0].querySelector('[kanban_uid_app]');

                var descripcion = ui.item.children()[1].querySelector('[kanban_descripcion_tarjeta]');

                var fechaCreacion = ui.item.children()[1].querySelector('[kanban_fecha_creacion]');

                var nombreResponsable = ui.item.children()[1].querySelector('[kanban_nombre_responsable]');

                var mailResponsable = ui.item.children()[1].querySelector('[kanban_mail_responsable]');

                var link = ui.item.children()[1].querySelector('[kanban_link_tarjeta]');

                var ultimaActualizacion = ui.item.children()[1].querySelector('[kanban_ultima_actualizacion]');

                if (estado !== null && estado !== "") {
                    data['estado'] = estado;
                    console.log("Estado: " + estado);
                }

                if (uidEstado !== null && uidEstado !== "") {
                    data['uidEstado'] = uidEstado;
                    console.log("Uid estado: " + estado);
                }

                if (servicioCambioEstado !== null && servicioCambioEstado !== "") {
                    console.log("Servicio para cambio de estado en la BD: " + servicioCambioEstado);
                }

                if (uidTarjeta !== null && uidTarjeta !== "") {
                    data['uidTarjeta'] = uidTarjeta.getAttribute('kanban_uid_tarjeta');
                    console.log("Uid tarjeta: " + uidTarjeta.getAttribute('kanban_uid_tarjeta'));
                }

                if (codigoTarjeta !== null && codigoTarjeta !== "") {
                    data['codigoTarjeta'] = codigoTarjeta.getAttribute('kanban_codigo_tarjeta');
                    console.log("Codigo tarjeta: " + codigoTarjeta.getAttribute('kanban_codigo_tarjeta'));
                }

                if (uidApp !== null && uidApp !== "") {
                    data['uidApp'] = uidApp.getAttribute('kanban_uid_app');
                    console.log("Uid App: " + uidApp.getAttribute('kanban_uid_app'));
                }

                if (descripcion !== null && descripcion!== "") {
                    data['descripcion'] = descripcion.getAttribute("kanban_descripcion_tarjeta");
                    console.log("Descripcion: " + descripcion.getAttribute("kanban_descripcion_tarjeta"));
                }

                if (fechaCreacion !== null && fechaCreacion !== "") {
                    data['fechaCreacion'] = fechaCreacion.getAttribute('kanban_fecha_creacion');
                    console.log("Fecha creaciom: " + fechaCreacion.getAttribute('kanban_fecha_creacion'));
                }

                if (nombreResponsable !== null && nombreResponsable !== "") {
                    data['nombreResponsable'] =nombreResponsable.getAttribute("kanban_nombre_responsable");
                    console.log("Nombre responsable: " + nombreResponsable.getAttribute("kanban_nombre_responsable"));
                }

                if (mailResponsable !== null && mailResponsable !== "") {
                    data['mailResponsable'] = mailResponsable.getAttribute("kanban_mail_responsable");
                    console.log("Mail responsable: " + mailResponsable.getAttribute("kanban_mail_responsable"));
                }

                if (link !== null && link !== "") {
                    data['link'] = link.getAttribute("kanban_link_tarjeta");
                    console.log("Link: " + link.getAttribute("kanban_link_tarjeta"));
                }

                if (ultimaActualizacion !== null && ultimaActualizacion !== "") {
                    data['ultimaActualizacion'] = ultimaActualizacion.getAttribute("kanban_ultima_actualizacion");
                    console.log("Ultima Actualizacion: " + ultimaActualizacion.getAttribute("kanban_ultima_actualizacion"));
                }

                var arregloJson = {
                    status: "success",
                    data: data
                };

                var jsonTarjeta = JSON.stringify(arregloJson);

                console.log(jsonTarjeta);

                var parametros = {
                    "estadoTarjeta" : jsonTarjeta
                };

                $.ajax({
                    type: "POST",
                    //contentType: "application/json; charset=utf-8",
                    url: servicioCambioEstado,
                    dataType: "json",
                    data: parametros,
                    success: function (data){ },
                    error: function (data){ }
                });
            }
        });
    });
}



/*
$(".contenedorTarjetas").droppable({
    drop: function() {
        console.log("Elemento agregado");
    }
});*/

//Permite obtener id del boton que despliega el modal para insertar elementos arrastrables antes que el enla funcion agregarElemento
var idBoton;
$(".botonObtenerId").click(function() {
    idBoton = $(this).attr("id");
});

//Permite obtener id de la columna en la cual se desea agregar un nuevo elemento
var idColumna;
$(".botonAgregarElemento").click(function() {
    idColumna = $(this).parent().parent().attr("id");
    console.log("Identificador columna: " + idColumna);
    var contenedorIngreso = $('#'+idColumna).children()[1].getAttribute("id");

    /*
    var contenedorIngreso2 = columnaIngreso2.childNodes[1];
    console.log("El numero de hijos es " + contenedorIngreso2[0].length);
    console.log("Y el contenedor para guardar es " + contenedorIngreso2.getAttribute("id")); */
});

//Mediante el boton cerrar, se obtiene el id del elemento arrastrable navegando por cada uno de los nodos padres
var idContenedor2;
$(".botonEliminar").click(function() {
    idContenedor2 = $(this).parent().parent().parent().attr("id");
    var contenedor = document.getElementById(idContenedor2);
    contenedor.parentNode.removeChild(contenedor);
});


function agregarContenedor() {
    //Cuenta todos los id's que comienzan con la palabra 'columna'
    var cuentaColumnas = $("div[id^=columna]").length;

    var titColumna = document.getElementById("nombreColumna").value;

    //Agrega una nueva columna
    var columna = document.createElement("div");
    columna.setAttribute("id","columna"+(cuentaColumnas+1)); //Asigna un id a la columna utilizando un numero mas en id
    columna.setAttribute("class","columna");

    var divTitulo = document.createElement("div");
    divTitulo.setAttribute("id","titulo"+(cuentaColumnas+1));
    divTitulo.setAttribute("class","contTituloColumna");

    var tituloColumna = document.createTextNode(titColumna);
    var encabezadoTitulo = document.createElement("h4");
    encabezadoTitulo.appendChild(tituloColumna);
    divTitulo.appendChild(encabezadoTitulo);

    var contenedor = document.createElement("div");
    contenedor.setAttribute("id","contenedor"+(cuentaColumnas+1));
    contenedor.setAttribute("class","contenedor connectedSortable");

    var divBoton = document.createElement("div");
    divBoton.setAttribute("id","contBoton"+(cuentaColumnas+1));
    divBoton.setAttribute("class","contBoton");

    var botonAgregaCol = document.createElement("button");
    botonAgregaCol.setAttribute("id","boton"+(cuentaColumnas+1));
    botonAgregaCol.setAttribute("type","button");
    botonAgregaCol.setAttribute("class","btn btn-primary btn-lg botonAgregarElemento botonObtenerId");
    botonAgregaCol.setAttribute("data-toggle","modal");
    botonAgregaCol.setAttribute("data-target","#modalElemento");

    var tituloBoton = document.createTextNode("Agregar Elemento");

    botonAgregaCol.appendChild(tituloBoton);
    divBoton.appendChild(botonAgregaCol);

    columna.appendChild(divTitulo);
    columna.appendChild(contenedor);
    columna.appendChild(divBoton);

    var nodoPadre = document.getElementById("contPrincipal");
    nodoPadre.appendChild(columna);

    //Permite desplazar elementos arrastrables a los div con clase connectedSortable
    $( ".connectedSortable" ).sortable({
        connectWith: ".connectedSortable"
    });

    //Permite obtener id de la columna en la cual se desea agregar un nuevo elemento
    $(".botonAgregarElemento").click(function() {
        idColumna = $(this).parent().parent().attr("id");
        console.log("Identificador columna: " + idColumna);
    });
}


function agregarElemento() {
    var cuentaElementos = $("div[id^=elemento]").length; //Obtiene el numero del elementos arrastrables hasta el momento

    // Obtiene los input del formulario modal
    var campoTitulo = document.getElementById("campoTitulo").value;
    var campoContenido = $("#campoContenido").val();
    console.log(campoContenido);

    //Crea nuevo panel
    var panel = document.createElement("div");
    panel.setAttribute("class", "panel panel-info elemento");
    panel.setAttribute("id","elemento"+(cuentaElementos+1));

    //Crea encabezado para el nuevo panel
    var encabezadoPanel = document.createElement("div");
    encabezadoPanel.setAttribute("class", "panel-heading");
    var contEncabezado = document.createElement("h4");
    contEncabezado.setAttribute("class", "panel-title");
    var titulo = document.createTextNode(campoTitulo);
    contEncabezado.appendChild(titulo);
    var botonEliminar = document.createElement("button");
    botonEliminar.setAttribute("type","button");
    botonEliminar.setAttribute("class","close botonEliminar");
    botonEliminar.setAttribute("aria-hidden","true");
    var textoBoton = document.createTextNode("x");
    botonEliminar.appendChild(textoBoton);
    contEncabezado.appendChild(botonEliminar);
    encabezadoPanel.appendChild(contEncabezado);

    //Crea cuerpo para el nuevo panel
    var cuerpoPanel = document.createElement("div");
    cuerpoPanel.setAttribute("class", "panel-body");
    cuerpoPanel.setAttribute("id", 'contenido-elemento-' + (cuentaElementos+1));

    //Agrega el encabezado y cuerpo al panel
    panel.appendChild(encabezadoPanel);
    panel.appendChild(cuerpoPanel);

    /*
     Se obtiene el id de la columna en la cual se desea ingresar el elemento y a la vez obtenemos el div
     contenedor ubicado en la segunda posicion despues del div titulo
     */
    var idContenedor = $('#'+idColumna).children()[1].getAttribute("id");
    var contenedorIngreso = document.getElementById(idContenedor);

    contenedorIngreso.appendChild(panel);

    $('#contenido-elemento-' + (cuentaElementos+1)).html(campoContenido);

    //Mediante el boton cerrar, se obtiene el id del elemento arrastrable navegando por cada uno de los nodos padres
    $(".botonEliminar").click(function() {
        var idContenedor2 = $(this).parent().parent().parent().attr("id");
        var contenedor = document.getElementById(idContenedor2);
        contenedor.parentNode.removeChild(contenedor);
    });
}
