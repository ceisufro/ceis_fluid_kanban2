/**
 * Created by javier on 18-03-16.
 */

//Permit bloquear ciertos comportamientos del navegador al momento del arrastre
function allowDrop(ev) {
    ev.preventDefault();
}

//Permite el envio de datos declrando con ello el tipo
function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

//Funcion para soltar elementos
function drop(ev) {
    //Bloque comportamientos del navegador el soltar elementos
    ev.preventDefault();
    //getData retorna los datos del tipo especifado
    var data = ev.dataTransfer.getData("text");
    //Se accede el contenedor receptor y se ubica el elemento arrastrado
    ev.target.appendChild(document.getElementById(data));
}

