<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}


\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Francisco.DragAndDropV2', 'Content');
\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Francisco.DragAndDropV2', 'Backend');

$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['estadoTarjeta'] = 'EXT:drag_and_drop_v2/Classes/EID/cambioEstadoTarjeta.php';
