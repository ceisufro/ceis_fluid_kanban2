<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "Francisco.DragAndDropV2".
 *
 * Auto generated 05-04-2016 15:57
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'DragAndDropV2',
	'description' => 'DragAndDropV2',
	'category' => 'misc',
	'shy' => 0,
	'version' => '0.0.2',
	'dependencies' => 'cms,extbase,fluid,flux,fluidcontent,vhs',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'state' => 'experimental',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author' => 'f.yanquin01@ufromail.cl',
	'author_email' => '',
	'author_company' => '',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'constraints' => array(
		'depends' => array(
			'typo3' => '4.5-7.6.99',
			'cms' => '',
			'extbase' => '',
			'fluid' => '',
			'flux' => '',
			'fluidcontent' => '',
			'vhs' => '',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:0:{}',
	'suggests' => array(
	),
);
